const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Category } = require("../../models");

const getOneCategory = async (categoryId) => {
  let data = null;
  const entityCache = new EntityCache("Category", []);
  data = await entityCache.getEntityFromCache(categoryId);
  if (data) data.source = "EntityCache";

  if (!data) {
    const category = await Category.findById(categoryId);
    if (category) data = category.getData();
    if (data) data.source = "db";
  }
  return data;
};

const getCategories = async () => {
  let data = [];
  const categories = await Category.find();
  if (categories) {
    data = categories.map((category) => category.getData());
    data.source = "db";
  }
  return data;
};

const getCategory = async (req, res, next) => {
  const categoryId = req.params.categoryId;
  try {
    let data = null;
    if (categoryId) {
      data = await getOneCategory(categoryId);
      if (!data) {
        return next(new HttpError(404, "Category Not Found", 0, ""));
      }
    } else {
      data = await getCategories();
    }
    res.status(200).send(data);
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Getting Category", 0, err.message)
    );
  }
};

module.exports = getCategory;
