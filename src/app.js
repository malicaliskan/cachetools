const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");

const { errorHandler } = require("./utils");

const app = express();

app.use(cookieParser());
app.use(cors());
app.use(express.json());

const {
  brandRouter,
  categoryRouter,
  productRouter,
} = require("./demo/routers");

app.use(brandRouter);
app.use(categoryRouter);
app.use(productRouter);

app.get("/*", (req, res) => {
  res.send("hello, this is cache-tools-service");
});

app.use(errorHandler);

module.exports = app;
