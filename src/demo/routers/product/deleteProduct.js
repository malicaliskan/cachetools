const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Product } = require("../../models");

const deleteProduct = async (req, res, next) => {
  const productId = req.params.productId;

  try {
    const product = await Product.findById(productId);
    if (!product) {
      return next(new HttpError(404, "Product Not Found To Delete", 0, ""));
    }

    await Product.findByIdAndDelete(productId);

    const entityCache = new EntityCache("Product", ["brandId", "categoryId"]);
    await entityCache.delEntityFromCache(productId);

    const elasticIndexer = new ElasticIndexer('product_index');
    await elasticIndexer.deleteData(productId);

    res.status(200).send({ id: productId });
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Deleting A Product", 0, err.message)
    );
  }
};

module.exports = deleteProduct;
