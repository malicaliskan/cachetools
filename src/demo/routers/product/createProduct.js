const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Product } = require("../../models");

const populateQuery = require("./populate");

const createProduct = async (req, res, next) => {
  const productName = req.body.productName;
  const price = req.body.price;
  const brandId = req.body.brandId;
  const categoryId = req.body.categoryId;

  if (!productName)
    return next(new HttpError(400, "productName is Required", 0, ""));
  if (!price) return next(new HttpError(400, "price is Required", 0, ""));
  if (!brandId) return next(new HttpError(400, "brandId is Required", 0, ""));
  if (!categoryId)
    return next(new HttpError(400, "categoryId is Required", 0, ""));

  try {
    const product = await Product.create({
      name: productName,
      price: price,
      brandId: brandId,
      categoryId: categoryId,
    });
    let data = product.getData();
    const entityCache = new EntityCache("Product", ["brandId", "categoryId"]);
    await entityCache.saveEntityToCache(data);

    let mergedProduct = Product.findById(data.id);
    mergedProduct = populateQuery(mergedProduct);
    mergedProduct = await mergedProduct.exec();
    data = mergedProduct.getData();

    const elasticIndexer = new ElasticIndexer('product_index');
    await elasticIndexer.indexData(data);

    res.status(201).send(data);
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Creating A Product", 0, err.message)
    );
  }
};

module.exports = createProduct;
