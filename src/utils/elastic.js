const { Client } = require("@elastic/elasticsearch");
const elasticUri = process.env.ELASTIC_URI || "http://localhost:9200";
const elasticUser = process.env.ELASTIC_USER || "elastic";
const elasticPwd = process.env.ELASTIC_PWD || "zci+imLCfkbSC=RxLHjH";

let elasticClient = null;
try {
  elasticClient = new Client({
    node: elasticUri,
    auth: { username: elasticUser, password: elasticPwd },
    ssl: {
      ca: process.env.ELASTIC_CERT,
      rejectUnauthorized: false,
    },
  });
} catch (err) {
  console.log("elasticClient can not be created", err.message);
}

const connectToElastic = async () => {
  //
};

const closeElastic = () => {
  elasticClient.close();
};

module.exports = { connectToElastic, closeElastic, elasticClient };
