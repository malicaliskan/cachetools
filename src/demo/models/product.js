const { mongoose } = require("../../utils");
const { Schema } = require("mongoose");

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    brandId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "Brand",
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "Category",
    },
  },
  {
    methods: {
      getData() {
        let ret = {};
        ret.id = this._doc._id.toString();
        Object.assign(ret, this._doc);
        delete ret._id;
        return ret;
      },
    },
  }
);

productSchema.methods.getData = function () {
  let ret = {};
  ret.id = this._doc._id.toString();
  const docProps = Object.keys(this._doc).filter((key) => key != "_id");
  // copy all props from doc
  docProps.forEach((propName) => (ret[propName] = this._doc[propName]));
  ret.category = this.category ? this.category.getData() : this.category;
  ret.brand = this.brand ? this.brand.getData() : this.brand;

  return ret;
};

productSchema.set("versionKey", "recordVersion");
productSchema.set("timestamps", true);

productSchema.virtual("category", {
  ref: "Category",
  localField: "categoryId",
  foreignField: "_id",
  justOne: true,
});

productSchema.virtual("brand", {
  ref: "Brand",
  localField: "brandId",
  foreignField: "_id",
  justOne: true,
});

module.exports = mongoose.model("Product", productSchema);
