const { mongoose } = require("../../utils");

const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    }
  },
  {
    methods: {
      getData() {
        let ret = {};
        ret.id = this._doc._id.toString();
        Object.assign(ret, this._doc);
        delete ret._id;
        return ret;
      },
    },
  }
);

categorySchema.set("versionKey", "recordVersion");
categorySchema.set("timestamps", true);

module.exports = mongoose.model("Category", categorySchema);
