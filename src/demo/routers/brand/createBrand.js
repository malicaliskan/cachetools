const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Brand } = require("../../models");

const createBrand = async (req, res, next) => {
  const brandName = req.body.brandName;

  if (!brandName)
    return next(new HttpError(400, "BrandName is Required", 0, ""));

  try {
    const brand = await Brand.create({
      name: brandName,
    });
    const data = brand.getData();
    const entityCache = new EntityCache("Brand", []);
    await entityCache.saveEntityToCache(data);
    res.status(201).send(data);
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Creating A Brand", 0, err.message)
    );
  }
};

module.exports = createBrand;
