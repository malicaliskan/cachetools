const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Brand } = require("../../models");

const { brandDeleted } = require("../product/productIndexUpdate");

const deleteBrand = async (req, res, next) => {
  const brandId = req.params.brandId;

  try {
    const brand = await Brand.findById(brandId);
    if (!brand) {
      return next(new HttpError(404, "Brand Not Found To Delete", 0, ""));
    }

    await Brand.findByIdAndDelete(brandId);

    const entityCache = new EntityCache("Brand", []);
    await entityCache.delEntityFromCache(brandId);

    await brandDeleted(brandId);

    res.status(200).send({ id: brandId });
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Deleting A Brand", 0, err.message)
    );
  }
};

module.exports = deleteBrand;
