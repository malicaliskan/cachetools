module.exports = {
  Brand: require("./brand"),
  Category: require("./category"),
  Product: require("./product"),
};
