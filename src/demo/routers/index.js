module.exports = {
  brandRouter: require("./brand"),
  categoryRouter: require("./category"),
  productRouter: require("./product"),
};
