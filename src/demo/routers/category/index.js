const express = require("express");
const router = express.Router();

router.get("/categories/:categoryId", require("./getCategory"));
router.get("/categories", require("./getCategory"));
router.post("/categories/", require("./createCategory"));
router.patch("/categories/:categoryId", require("./updateCategory"));
router.delete("/categories/:categoryId", require("./deleteCategory"));

module.exports = router;
