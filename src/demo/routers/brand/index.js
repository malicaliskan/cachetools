const express = require("express");
const router = express.Router();

router.get("/brands/:brandId", require("./getBrand"));
router.get("/brands", require("./getBrand"));
router.post("/brands/", require("./createBrand"));
router.patch("/brands/:brandId", require("./updateBrand"));
router.delete("/brands/:brandId", require("./deleteBrand"));

module.exports = router;
