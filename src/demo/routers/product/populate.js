const populateQuery = (query) => {
  query = query.populate([
    {
      path: "category",
      select: "_id name",
      model: "Category",
    },
    {
      path: "brand",
      select: "_id name",
      model: "Brand",
    },
  ]);
  return query;
};
module.exports = populateQuery;
