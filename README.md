# CacheTools

CacheTols Provides 4 utility classes to handle all caching types and strategies that is described in this medium article

[The Essential Guide to Caching in Microservices Architecture](https://medium.com/hexaworks-papers/the-essential-guide-to-caching-in-microservices-architecture-b10816e8655d)

## Getting started

Clone the projec in your local repository and oğen with your favorite IDE.

First open an integrated terminal and install npm packages

```
npm i -y
```

You will see 3 files in /src/cache-tools directory. They include 3 main classes to manage the caching.

**ElasticIndexer** : Use this class to manage aggregation caching. This class will index your aggreagated entity and will update any joined data when chnaged. 

In demo project, product data that is agregated with category and brand data is indexed with this class.

**EnityCache**: This is a great class to use Redis for entity caching. Use this class for caching most used data pure data. 

In demo project, brand data, and category data is indexed with this class. Note that these datas dont need extra agrregations and tehy are used frequently by other business parts of an application.

**QueryCache**: This class is used to cache the result of bulk queries and then invalidating the cache when the data changes. It also have a second class **QueryCacheInvalidater** class to define the invalidate clusters.

Demo project doesnt include a usage for this class, it will be added soon.

## Test and Deploy

To be able to use this demo, please

1. install a MongoDb server or arrange a MongoDb Cloud location
Don't forget to set your own MongoDb configs in .dev.env file.

2. Install a Redis server
Don't forget to set your own Redis configs in .dev.env file.

3. Install an ElasticSearch Server
Don't forget to set your own ElasticSearch configs in .dev.env file.

4. You can alternatively test the application using postman. A detailed postman collection is provided in this repository. Find it near this read.me file.

## Support
Please don't hesitate to ask for any questions to me.

Mehmed Ali Çalışkan
Email: malicaliskan@gmail.com

Before asking questions you can check my article in medium about caching:
[The Essential Guide to Caching in Microservices Architecture](https://medium.com/hexaworks-papers/the-essential-guide-to-caching-in-microservices-architecture-b10816e8655d)

## Roadmap
These classes will be developed and include many exiting featues to provide a full caching toolset.
Demo project will include the usage of QueryCache classes.

## Contributing
We are open to contributions. Fork your own project and show us what can be done.

## Authors and acknowledgment
Mehmed Ali Çalışkan
I am the founder and chief architect of Hexaworks that makes amazing microservice based modern saas applications.

## License
It is open source. You can use it even without crediting.