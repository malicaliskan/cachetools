const { connectToElastic, closeElastic, elasticClient } = require("./elastic");
const { redisClient, connectToRedis, getRedisData } = require("./redis");
const { errorHandler, HttpError} = require("./errorHandler");
const {
  mongoose,
  connectToMongoDb,
  closeMongoDbConnection,
} = require("./mongo");

module.exports = {
  connectToElastic,
  closeElastic,
  elasticClient,
  getRedisData,
  redisClient,
  connectToRedis,
  mongoose,
  connectToMongoDb,
  closeMongoDbConnection,
  errorHandler,
  HttpError
};
