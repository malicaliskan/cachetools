const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Brand } = require("../../models");

const { brandUpdated } = require("../product/productIndexUpdate");

const updateBrand = async (req, res, next) => {
  const brandId = req.params.brandId;

  const brandName = req.body.brandName;
  if (!brandName)
    return next(new HttpError(400, "BrandName is Required", 0, ""));

  try {
    const brand = await Brand.findByIdAndUpdate(
      brandId,
      { name: brandName },
      { new: true }
    );
    if (!brand) {
      return next(new HttpError(404, "Brand Not Found To Update", 0, ""));
    }
    const data = brand.getData();
    const entityCache = new EntityCache("Brand", []);
    await entityCache.saveEntityToCache(data);
    await brandUpdated(data);
    res.status(200).send(data);
  } catch (err) {

    return next(
      new HttpError(500, "Db Error When Updating A Brand", 0, err.message)
    );
  }
};

module.exports = updateBrand;
