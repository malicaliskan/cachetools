const { mongoose } = require("../../utils");

const brandSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    }
  },
  {
    methods: {
      getData() {
        let ret = {};
        ret.id = this._doc._id.toString();;
        Object.assign(ret, this._doc);
        delete ret._id;
        return ret;
      },
    },
  }
);

brandSchema.set("versionKey", "recordVersion");
brandSchema.set("timestamps", true);

module.exports = mongoose.model("Brand", brandSchema);
