const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Brand } = require("../../models");

const getOneBrand = async (brandId) => {
  let data = null;
  const entityCache = new EntityCache("Brand", []);
  data = await entityCache.getEntityFromCache(brandId);
  if (data) data.source = "EntityCache";

  if (!data) {
    const brand = await Brand.findById(brandId);
    if (brand) data = brand.getData();
    if (data) data.source = "db";
  }
  return data;
};

const getBrands = async () => {
  let data = [];
  const brands = await Brand.find();
  if (brands) {
    data = brands.map((brand) => brand.getData());
  }
  return data;
};

const getBrand = async (req, res, next) => {
  const brandId = req.params.brandId;
  try {
    let data = null;
    if (brandId) {
      data = await getOneBrand(brandId);
      if (!data) {
        return next(new HttpError(404, "Brand Not Found", 0, ""));
      }
    } else {
      data = await getBrands();
    }
    res.status(200).send(data);
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Getting Brand", 0, err.message)
    );
  }
};

module.exports = getBrand;
