const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Category } = require("../../models");

const { categoryDeleted } = require("../product/productIndexUpdate");

const deleteCategory = async (req, res, next) => {
  const categoryId = req.params.categoryId;

  try {
    const category = await Category.findById(categoryId);
    if (!category) {
      return next(new HttpError(404, "Category Not Found To Delete", 0, ""));
    }

    await Category.findByIdAndDelete(categoryId);

    const entityCache = new EntityCache("Category", []);
    await entityCache.delEntityFromCache(categoryId);
    await categoryDeleted(categoryId);
    res.status(200).send({ id: categoryId });
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Deleting A Category", 0, err.message)
    );
  }
};

module.exports = deleteCategory;
