const {
    EntityCache,
    ElasticIndexer,
    QueryCache,
    QueryCacheInvalidator,
    HttpError,
  } = require("../imports");
  
  const { Product } = require("../../models");

  const populateQuery = require('./populate');
  
  const updateProduct = async (req, res, next) => {
    const productId = req.params.productId;
  
    const productName = req.body.productName;
    const price = req.body.price;

    if (!productName && !price)
      return next(new HttpError(400, "productName or price is Required", 0, ""));
  
    try {
      const product = await Product.findByIdAndUpdate(
        productId,
        { name: productName, price:price },
        { new: true }
      );
      if (!product) {
        return next(new HttpError(404, "Product Not Found To Update", 0, ""));
      }
      let data = product.getData();
      const entityCache = new EntityCache("Product", ["brandId", "categoryId"]);
      await entityCache.saveEntityToCache(data);


      let mergedProduct = Product.findById(data.id);
      mergedProduct = populateQuery(mergedProduct);
      mergedProduct = await mergedProduct.exec();
      data = mergedProduct.getData();
  
      const elasticIndexer = new ElasticIndexer('product_index');
      await elasticIndexer.indexData(data);

      res.status(200).send(data);
    } catch (err) {
      return next(
        new HttpError(500, "Db Error When Updating A Product", 0, err.message)
      );
    }
  };
  
  module.exports = updateProduct;
  