const { createClient } = require("redis");

const redisUri = process.env.REDIS_URI || "redis://127.0.0.1:6379";
const redisClient = createClient({ url: redisUri });
redisClient.on("error", (err) => console.log("Redis Client Error", err));

const getRedisData = async (key) => {
  try {
    let data = null;
    const dataStr = await redisClient.get(key);
    if (dataStr) {
      data = JSON.parse(dataStr);
    }
    return data;
  } catch (err) {
    console.log(err.message);
    return null;
  }
};

const connectToRedis = async () => {
  try {
    await redisClient.connect();
    console.log("connected to redis", redisUri);
  } catch (err) {
    console.log("cannot connect redis", redisUri);
  }
};

module.exports = { redisClient, connectToRedis, getRedisData };
