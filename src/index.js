if (process.env.NODE_ENV == "development")
  require("dotenv").config({ path: ".dev.env" });

if (!process.env.MONGO_URI) throw new Error("MONGO_URI must be defined");
if (!process.env.REDIS_URI) throw new Error("REDIS_URI must be defined");
if (!process.env.ELASTIC_URI) throw new Error("ELASTIC_URI must be defined");

const expressApp = require("./app");

const {
  connectToMongoDb,
  closeMongoDbConnection,
  connectToElastic,
  closeElastic,
  connectToRedis,
  redisClient
} = require("./utils");

let expressServer = null;

let processActive = false;

const start = async () => {
  process.title = "node-cache-tools-service";
  await connectToMongoDb();
  await connectToRedis();
  await connectToElastic();
  const servicePort = process.env.HTTP_PORT ?? 3000;
  expressServer = expressApp.listen(servicePort);
  console.log(
    "cache-tools-service is listening HTTP/REST port " +
      servicePort.toString()
  );
  processActive = true;
};

const signals = ["SIGINT", "SIGTERM", "SIGQUIT"];

const close = async (signal) => {
  try {
    console.log("Caught termination signal:", signal);
    if (processActive) {
      console.log("Closing all connections...");
      await expressServer.close();
      await closeMongoDbConnection();
      await closeElastic();
      await redisClient.quit();
      console.log("Active handles:", process._getActiveHandles().length);
      console.log("Closed all connections gracefully...Bye!");
      processActive = false;
    } else {
      console.log("Connections already closed...");
    }
  } catch (err) {
    process.exit(1);
  }
};

for (const signal of signals) {
  process.on(signal, async () => {
    if (!signals.includes(signal)) return;
    await close(signal);
  });
}

start();
