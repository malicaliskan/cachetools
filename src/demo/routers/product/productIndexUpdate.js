const { ElasticIndexer } = require("../imports");

const categoryUpdated = async (data) => {
  const elasticIndexer = new ElasticIndexer("product_index");
  await elasticIndexer.updateIndex(
    { "category.id": data.id },
    `ctx._source.category.name = '${data.name}'`
  );
};

const brandUpdated = async (data) => {
  const elasticIndexer = new ElasticIndexer("product_index");
  await elasticIndexer.updateIndex(
    { "brand.id": data.id },
    `ctx._source.brand.name = '${data.name}'`
  );
};

const categoryDeleted = async (id) => {
  const elasticIndexer = new ElasticIndexer("product_index");
  await elasticIndexer.updateIndex(
    { "category.id": id },
    `ctx._source.category.name = null`
  );
};

const brandDeleted = async (id) => {
  const elasticIndexer = new ElasticIndexer("product_index");
  await elasticIndexer.updateIndex(
    { "brand.id": id },
    `ctx._source.brand.name = null`
  );
};

module.exports = {
  categoryUpdated,
  brandUpdated,
  categoryDeleted,
  brandDeleted,
};
