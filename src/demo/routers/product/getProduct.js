const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Product } = require("../../models");

const populateQuery = require("./populate");

const getOneProduct = async (productId) => {
  let data = null;
  const elasticIndexer = new ElasticIndexer("product_index");
  data = await elasticIndexer.getDataById(productId);
  if (data) data.source = "Elasticsearch";
  if (!data) {
    let product = Product.findById(productId);
    product = populateQuery(product);
    product = await product.exec();
    if (product) data = product.getData();
    if (data) data.source = "db";
  }
  return data;
};

const getProducts = async () => {
  let data = null;
  const elasticIndexer = new ElasticIndexer("product_index");
  data = await elasticIndexer.getDataByPage(0, 1000);
  if (data && data.length > 0) {
    data.forEach((item) => (item.source = "elastic search"));
  }
  if (!data || data.length == 0) {
    const products = await Product.find();
    if (products) {
      data = products.map((product) => product.getData());
      data.forEach((item) => (item.source = "db"));
    }
  }
  return data;
};

const getProduct = async (req, res, next) => {
  const productId = req.params.productId;
  try {
    let data = null;
    if (productId) {
      data = await getOneProduct(productId);
      if (!data) {
        return next(new HttpError(404, "Product Not Found", 0, ""));
      }
    } else {
      data = await getProducts();
    }
    res.status(200).send(data);
  } catch (err) {
    console.log(err);
    return next(
      new HttpError(500, "Db Error When Getting Product", 0, err.message)
    );
  }
};

module.exports = getProduct;
