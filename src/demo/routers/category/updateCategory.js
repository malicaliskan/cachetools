const {
  EntityCache,
  ElasticIndexer,
  QueryCache,
  QueryCacheInvalidator,
  HttpError,
} = require("../imports");

const { Category } = require("../../models");

const { categoryUpdated } = require("../product/productIndexUpdate");

const updateCategory = async (req, res, next) => {
  const categoryId = req.params.categoryId;

  const categoryName = req.body.categoryName;
  if (!categoryName)
    return next(new HttpError(400, "CategoryName is Required", 0, ""));

  try {
    const category = await Category.findByIdAndUpdate(
      categoryId,
      { name: categoryName },
      { new: true }
    );
    if (!category) {
      return next(new HttpError(404, "Category Not Found To Update", 0, ""));
    }
    const data = category.getData();
    const entityCache = new EntityCache("Category", []);
    await entityCache.saveEntityToCache(data);
    await categoryUpdated(data);
    res.status(200).send(data);
  } catch (err) {
    return next(
      new HttpError(500, "Db Error When Updating A Category", 0, err.message)
    );
  }
};

module.exports = updateCategory;
