const {
    EntityCache,
    ElasticIndexer,
    QueryCache,
    QueryCacheInvalidator,
    HttpError,
  } = require("../imports");
  
  const { Category } = require("../../models");
  
  const createCategory = async (req, res, next) => {
    const categoryName = req.body.categoryName;
  
    if (!categoryName)
      return next(new HttpError(400, "CategoryName is Required", 0, ""));
  
    try {
      const category = await Category.create({
        name: categoryName,
      });
      const data = category.getData();
      const entityCache = new EntityCache("Category", []);
      await entityCache.saveEntityToCache(data);
      res.status(201).send(data);
    } catch (err) {
      return next(
        new HttpError(500, "Db Error When Creating A Category", 0, err.message)
      );
    }
  };
  
  module.exports = createCategory;
  