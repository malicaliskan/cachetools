const { elasticClient } = require("../utils");

class ElasticIndexer {
  constructor(indexName) {
    this.indexName = indexName.toLowerCase();
  }

  async indexData(data) {
    const indexName = this.indexName;
    await elasticClient.index({
      index: indexName,
      id: data.id,
      body: data,
    });
  }

  async deleteData(id) {
    await elasticClient.delete({
      index: this.indexName,
      id: id,
    });
  }

  async getDataById(id) {
    const document = await elasticClient.get({
      index: this.indexName,
      id: id,
    });
    if (document && document.found && document._source) return document._source;
    return null;
  }

  async getDataByPage(from, size) {
    let result = [];
    const document = await elasticClient.search({
      index: this.indexName,
      from: from,
      size: size,
      query: {
        match_all: { boost: 1.2 },
      },
    });
    if (document && document.hits && document.hits.hits) {
      result = document.hits.hits.map((source) => source._source);
    }

    return result;
  }

  async updateIndex(match, script) {
    try {
      const result = await elasticClient.updateByQuery({
        index: this.indexName,
        query: { match: match },
        script: { inline: script },
      });

      return result;
    } catch (err) {
      console.log(err);
    }
  }
}

module.exports = ElasticIndexer;
