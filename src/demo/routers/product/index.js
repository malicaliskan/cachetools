const express = require("express");
const router = express.Router();

router.get("/products/:productId", require("./getProduct"));
router.get("/products", require("./getProduct"));
router.post("/products/", require("./createProduct"));
router.patch("/products/:productId", require("./updateProduct"));
router.delete("/products/:productId", require("./deleteProduct"));

module.exports = router;
