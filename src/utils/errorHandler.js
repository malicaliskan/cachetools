class HttpError extends Error {
  constructor(status, messsage, errorCode, detail) {
    super(messsage);
    this.status = status;
    this.errorCode = errorCode ?? status;
    this.detail = detail;
  }
}

const errorHandler = (err, req, res, next) => {
  let status = 500;
  let errCode = 500;
  let detail = null;

  if (err instanceof HttpError) {
    status = err.status;
    errCode = err.errorCode;
    detail = err.detail;
  }
  const date = new Date();
  const response = {
    result: "ERR",
    status: status,
    message: err.message,
    errCode: errCode,
    detail: detail,
    date: date.toISOString()
  }
  res.status(status).send(response);
};

module.exports = { errorHandler, HttpError };
